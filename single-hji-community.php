<?php while (have_posts()) : the_post(); ?>

    <?php
    
   
    
    $communityObj = get_post_meta( $post->ID, 'hji-community-obj', true );
    
    $communityObj = json_decode( $communityObj );
    
    
    $apiController = \hji\membership\Membership::getInstance()->apiController;

    $params = array('id' => $communityObj->id, 'details' => true);

    $result = $apiController->api->getAreas( $params );


    
    
    $lf_array = $result['areas'][0];
    
    if ( $lf_array ) {
    
        $pop_array = $lf_array['population'];
    
            $pop_current = $pop_array['current'];
    
            $pop_2000 = $pop_array['2000'];
    
            $pop_percent = round( ( ( ( $pop_current / $pop_2000 ) - 1 ) * 100 ), 0 );
    
        $house_array = $lf_array['households'];
    
            $house_total = number_format( $house_array['total'] );
    
            $house_children = round( $house_array['occupancy']['children'], 0 );

            $house_owned = round( $house_array['occupancy']['owned'], 0 );
            
            $house_rented = round( $house_array['occupancy']['rented'], 0 );
            
            $house_vacant = round( $house_array['occupancy']['vacant'], 0 );
            
            $house_value = number_format( $house_array['value'] );
            
            $house_year = $house_array['yearBuilt'];
            
    }

            
    

    $background = get_field('header_image');


    $site_url = get_bloginfo('template_directory');
    ?>

    <style>
        .community-header-block{
            background: url( '<?php echo $site_url; ?>/assets/img/bgs/neighborhood-shadow.png' ), url('<?php echo $background; ?>');
        }
    </style>

    <div class="community_content_wrap" id="communities">

        <article <?php post_class(); ?>>

            <div class="community-header-block">

                <header>

                    <h1 class="community-title"><?php the_title(); ?></h1>

                </header>

                <div class="community-header-content">

                    <?php
                    $header_text = get_field('header_text');

                    if ($header_text != '') {

                        echo $header_text;
                    } else {

                        the_excerpt();
                    }
                    ?>

                    <div class="header-page-links">

                        <span>Explore:</span>

                        <a href="#featured-properties">Featured Properties</a>

                        <a href="#neighborhood-info">Neighborhood Info</a>

                        <a href="#communities-section">Communities</a>

                    </div>

                </div>

            </div>

            <div class="featured-properties-block">

                <div class="communities-title-wrap">

                    <h1 id="featured-properties">Featured Properties</h1>

                    <?php $comm_title = get_the_title(); ?>


                
                    <?php 

                    $idx = get_field('idx_short_code');
                    
                    $a = substr( $idx, 1, -1 );
                    
                    $b = explode( '" ', $a );
                    
                    $listing_search_array = array();
                    
                    foreach ( $b as $c ) {
                        
                        $c = str_replace( '"', "", $c );
                    
                        $c = str_replace( "'", "", $c );
                        
                        $d = explode( '=', $c );
                        
                        $listing_search_array[$d[0]] = $d[1];
                    }
                                        
                    $city_var = $prop_type_var = $keyword_var = '';
                    
                    if ( isset( $listing_search_array['city'] ) ) {
                        
                        $city_var = $listing_search_array['city'];
                    }

                                        if ( isset( $listing_search_array['polygon'] ) ) {
                        
                        $poly_var = $listing_search_array['polygon'];
                    }
                    
                    if( isset( $listing_search_array['property_type'] ) ) {
                        
                        $prop_type_var = $listing_search_array['property_type'];
                    }
                    
                    if( isset( $listing_search_array['keyword'] ) ) {
                        
                        $keyword_var = $listing_search_array['keyword'];
                    }
                    
                    if( isset( $listing_search_array['subdivision'] ) ) {
                        
                        $subdivision_var = $listing_search_array['subdivision'];
                    }

                                        $community_title = get_the_title();
                    
                    
                    $search_link_string = site_url() . "/results/?property_type=" . $prop_type_var . "&polynames=" . $community_title . "&polygon=" . $poly_var . "&city-zip=" . $city_var . "&beds_total[]=&beds_total[]=&baths_total=&price[]=&price[]=&subdivision=&keyword=" . $keyword_var . "&subdivision=" . $subdivision_var  ."&living_area=&year_built=&mls=crmls&same_page_results=&idxs_do=";
                    
                    ?>
                  
                    <a class="btn btn-sc-gold featured-properties-link" href="<?php echo $search_link_string; ?>">View <?php echo $comm_title; ?> Listings <i class="fa fa-long-arrow-right"></i></a>

                </div>

                <div class="communities-ridx-wrap">
                    

                <?php echo do_shortcode( $idx ); ?>
                    
                </div>

            </div>


            <div class="neighborhood-info-block">

                <div class="communities-title-wrap">

                    <h1 id="neighborhood-info">Neighborhood Info</h1>

                </div>

                <div class="row">

                    <?php

                    $rs_shortcode = get_field( 'recent_sales' );

                    if ( $rs_shortcode != '' ) {

                        $ni_content = do_shortcode( $rs_shortcode );

                    } else {

                        $ni_content = get_the_content();
                    }

                    ?>
                    
                    <?php if ( ! $lf_array ) { ?>
                    
                    <div class="col-sm-12">

                        <?php echo $ni_content; ?>

                    </div>
                    
                    <?php } else { ?>

                    <div class="col-sm-8">

                        <?php echo $ni_content;
                        //echo do_shortcode( $ni_content ); ?>

                    </div>
                    
                    <div class="col-sm-3 neighborhood-details">
                                              
                        <?php
                                                   
                            if ( ( ( $pop_current ) != '' ) && ( ( $pop_percent ) != '' ) ) {
                            ?>

                            <div class="detail">

                                <div class="icon">

                                    <i class="fa fa-group"></i>

                                </div>

                                <div class="box">

                                    <div class="top">Population</div>

                                    <div class="big"><?php echo number_format( $pop_current ); ?></div>

                                    <div class="bottom">up <?php echo $pop_percent; ?>% since 2000</div>                    

                                </div>

                                <div class="pushbottom"></div>

                            </div>

                    <?php } 
                                                   
                        if ( ( ( $house_total ) != '' ) && ( ( $house_children ) != '' ) ) { 
                    ?>

                            <div class="detail">

                                <div class="icon">

                                    <i class="fa fa-home"></i>

                                </div>

                                <div class="box">

                                    <div class="top">Households</div>

                                    <div class="big"><?php echo $house_total; ?></div>

                                    <div class="bottom"><?php echo $house_children; ?>% with children</div>                    

                                </div>

                                <div class="pushbottom"></div>

                            </div>

                    <?php } if ( ( ( $house_owned ) != '' ) && ( ( $house_rented ) != '' ) && ( ( $house_vacant ) != '' ) ) { 
                    ?>

                            <div class="detail">

                                <div class="icon">

                                    <i class="fa fa-key"></i>

                                </div>

                                <div class="box">

                                    <div class="top">Ownership</div>

                                    <div class="big"><?php echo $house_owned; ?>% Owned</div>

                                    <div class="bottom"><?php echo $house_rented; ?>% rented, <?php echo $house_vacant; ?>% vacant</div>                    

                                </div>

                                <div class="pushbottom"></div>

                            </div>

                    <?php } if ( ( $house_value != '' ) && ( $house_value != 0 ) ) { ?>
                        
                            <div class="detail">

                                <div class="icon">

                                    <i class="fa fa-usd"></i>

                                </div>

                                <div class="box">

                                    <div class="top">Average Home Value</div>

                                    <div class="big">$<?php echo $house_value; ?></div>

                                    <div class="bottom"></div>                    

                                </div>

                                <div class="pushbottom"></div>

                            </div>

                    <?php } if ( $house_year != '' ) { ?>

                            <div class="detail">

                                <div class="icon">

                                    <i class="fa fa-calendar"></i>

                                </div>

                                <div class="box">

                                    <div class="top">Average Year Built</div>

                                    <div class="big"><?php echo $house_year; ?></div>

                                    <div class="bottom"></div>                    

                                </div>

                                <div class="pushbottom"></div>

                            </div>

                    <?php } ?>
                       
                        </div>
                    
                    <?php } ?>

                </div>

            </div>

            <div class="pushbottom"></div>


            <div class="communities-image-slider-bar">

                <!-- using jQuery to move slider into here -->

            </div>   

            <div class="neighborhood-info-block">

                <div class="communities-title-wrap">

                    <h1 id="communities-section">Communities</h1>

                </div>

                <br /><br />

                <div class="communities-content-wrap">

                    <?php
                    $communities_text = get_field( 'communities_text' );

                    if ( $communities_text != '') {

                        echo $communities_text;

                    } else {

                        the_excerpt();
                    }
                    ?>

                </div>
                
                <?php the_content(); ?>

                <div class="communities-map-wrapper-new">
                    
                    <div class="hji-communities-post-content">

                        <!-- Move Map to here with jQuery -->
                    
                    </div>

                </div>

                <div class="communities-child-properties row">

    <?php
    
    $old_comm_title = get_the_title();
    
    $args = array('post_type' => 'hji-community', 'post_parent' => $post->ID);

    $communities = new WP_Query($args);

    $i = 0;

    while ($communities->have_posts()) : $communities->the_post();

        if (!has_post_thumbnail()) {
            continue;
        }

        ++$i;

        if ($i < 4) {
            ?>

                            <div class="community-excerpt-wrapper">

                                <div class="col-sm-4 image-wrap">

                            <?php if (has_post_thumbnail ()) { ?>

                                    <a href="<?php the_permalink (); ?>">

            <?php the_post_thumbnail ('communities', array ('class' => 'img-responsive')); ?>

                                    </a>

            <?php } ?>

                                </div>

                                <div class="col-sm-8">

                                    <h2><a href="<?php the_permalink (); ?>"><?php the_title (); ?></a></h2>

            <?php $com_content = get_the_content (); ?>

            <?php echo hj_excerpt (300, $com_content, ''); ?>

                                    <br />

                                    <a class="read-more" href="<?php the_permalink (); ?>">... read more about <?php the_title (); ?></a>

                                </div>

                                <div class="pushbottom"></div>

                            </div>

            <?php
            
            } elseif ( $i == 4 ) {
            
                echo "<div class='extra-communities-label'>Other " . $old_comm_title . " Communities:</div>"; ?>
                
                    <span class="extra-communities"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span> 
                
            <?php } else { ?>
                
                <span class="extra-communities"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span> 
                           
           <?php } ?>
                

        <?php endwhile; wp_reset_postdata(); ?>

                </div>
        
                <div class="communities-buttons">
                    
                    <?php $site_name = get_bloginfo( 'name' ); ?>
                    
                    <h1>Interested in buying property in <?php echo $old_comm_title; ?>?</h1>
                    
                    <p>Search through our <?php echo $old_comm_title; ?> property listings, or contact a <?php echo $site_name; ?> agent today to discuss your needs in detail.</p>
                    
                    <div class="row">
                        
                        <div class="col-sm-5 col-sm-offset-1">
                            
                            <a class="btn btn-sc-gold" href="<?php echo $search_link_string; ?>"><i class="fa fa-search"></i> Search in <?php echo $old_comm_title; ?></a>
                            
                        </div>                 
                        
                        <div class="col-sm-5">
                            
                            <a class="btn btn-sc-gold" href="<?php echo site_url(); ?>/email-property-alert/"><i class="fa fa-envelope-o"></i> Free Email Alerts</a>
                            
                        </div>
                        
                    </div>
        
                </div>

            </div><!-- end communities info block -->
            
            


            
            <div class="communities-ridx-details">

                <div class="details-header">

                    <h4><a href="#">Back to Top <i class="fa fa-long-arrow-up"></i></a></h4>

                </div>

                <div class="row">

                    <div class="details-inner-left col-sm-6">

                        <!-- using jQuery to move ridx details here -->

                    </div>

                    <div class="details-inner-right col-sm-6">

                        <!-- using jQuery to move ridx details here -->

                    </div>

                </div>

            </div>   

        </article>

    </div>

<?php
endwhile;
