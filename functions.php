<?php
/**
 * Sets our SCSS constant for the improved build process
 *
 * @since Blvd 2.7.0
 */
if ( !defined( 'HJI_BLVD_SCSS' ) ) {
    define( 'HJI_BLVD_SCSS', true );
}

if ( !function_exists( 'hji_new_widgets' ) ) :
/**
 * Override main theme widget neighborhoods
 */
function hji_new_widgets() {
  // Sidebars
  
  register_sidebar( array(
    'id'            => 'home-search-widget',
    'name'          => __( 'Home Search Widget', 'hji-textdomain' ),
    'description'   => __( 'Place Quick Search Widget here for Home Page' ),
    'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
    'after_widget'  => "</div>",
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ));
  
  register_sidebar( array(
    'id'            => 'slider-widget',
    'name'          => __( 'Slider Widget', 'hji-textdomain' ),
    'description'   => __( 'Place Slider Content Widget here for Home Page' ),
    'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
    'after_widget'  => "</div>",
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ));
  

    
    register_sidebar( array(
    'id'            => 'blvd-homepageparallax',
    'name'          => __( 'Homepage Parallax', 'hji-textdomain' ),
    'description'   => __( 'Homepage Parallax Image' ),
    'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
    'after_widget'  => "</div>",
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ));
}
add_action( 'widgets_init', 'hji_new_widgets' );
endif;




/**
* Menus for neighborhoods 
*/

register_nav_menus(array(
    'riverside-nav' => 'Riverside Menu',
    'moreno-valley-nav' => 'Moreno Valley Menu',
    'corona-nav' => 'Corona Menu',
    'inland-empire-nav' => 'Inland Empire Menu',
    'coachella-valley-nav' => 'Coachella Valley Menu',
    'orange-county-nav' => 'Orange County Menu',
    'los-angeles-county-nav' => 'Los Angeles County Menu',
    'san-diego-county-nav' => 'San Diego County Menu',
));

add_filter('hji_wp_nav_menu_item', 'dreambig_featured_menu_items', 10, 2);
function dreambig_featured_menu_items($item_html, $item)
{
    $item_output = $item_html;
    $arr_url = explode('/',$item->url);
    $totalelement = count($arr_url);
    $ind = $totalelement-2;
    if($arr_url[$ind]=='riverside-neighborhoods'){
        $item_output .= wp_nav_menu( array('container' => false,
            //'container_class' => 'sub-area',
            'theme_location' => 'riverside-nav',
            'menu_class' => 'sub-menu',
            'echo'            => false,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
        ) );
    }
    elseif($arr_url[$ind]=='moreno-valley-neighborhoods'){
        $item_output .= wp_nav_menu( array('container' => false,
            //'container_class' => 'sub-area',
            'theme_location' => 'moreno-valley-nav',
            'menu_class' => 'sub-menu',
            'echo'            => false,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
        ) );
    }
    elseif($arr_url[$ind]=='corona-neighborhoods'){
        $item_output .= wp_nav_menu( array('container' => false,
            //'container_class' => 'sub-area',
            'theme_location' => 'corona-nav',
            'menu_class' => 'sub-menu',
            'echo'            => false,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
        ) );
    }
    
    elseif($arr_url[$ind]=='inland-empire-regions'){
        $item_output .= wp_nav_menu( array('container' => false,
            //'container_class' => 'sub-area',
            'theme_location' => 'inland-empire-nav',
            'menu_class' => 'sub-menu',
            'echo'            => false,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
        ) );
    }
    
     elseif($arr_url[$ind]=='coachella-valley-regions'){
        $item_output .= wp_nav_menu( array('container' => false,
            //'container_class' => 'sub-area',
            'theme_location' => 'coachella-valley-nav',
            'menu_class' => 'sub-menu',
            'echo'            => false,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
        ) );
    }
    
    elseif($arr_url[$ind]=='orange-county-regions'){
        $item_output .= wp_nav_menu( array('container' => false,
            //'container_class' => 'sub-area',
            'theme_location' => 'orange-county-nav',
            'menu_class' => 'sub-menu',
            'echo'            => false,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
        ) );
    }
    
    elseif($arr_url[$ind]=='los-angeles-county-regions'){
        $item_output .= wp_nav_menu( array('container' => false,
            //'container_class' => 'sub-area',
            'theme_location' => 'los-angeles-county-nav',
            'menu_class' => 'sub-menu',
            'echo'            => false,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
        ) );
    } 
     
    elseif($arr_url[$ind]=='san-diego-county-regions'){
        $item_output .= wp_nav_menu( array('container' => false,
            //'container_class' => 'sub-area',
            'theme_location' => 'san-diego-county-nav',
            'menu_class' => 'sub-menu',
            'echo'            => false,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
        ) );
    }
    
    return $item_output;
}