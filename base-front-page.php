<!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <?php get_header( hji_theme_template_base() ); ?>

    <body <?php body_class(); ?>>

        <!--[if lt IE 9]>
            <div class="alert alert-warning">
                <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'hji-textdomain'); ?>
            </div>
        <![endif]-->

        <div id="wrapper" class="body-wrapper">
            
            <?php do_action( 'hji_theme_before_navbar' ); ?>
            
            <?php get_template_part( 'templates/header-navbar' ); ?>
            
            <?php do_action( 'hji_theme_after_navbar' ); ?>

            <div class="blvd-slideshow"></div>

            <section id="primary" class="primary-wrapper container">

                <div class="row slide-content">
                
                    <?php do_action( 'hji_theme_before_content' ); ?>                    

                    <div id="content" class="<?php echo hji_theme_main_class(); ?>" role="main">

                        <?php do_action( 'hji_theme_before_content_col' ); ?>

                        <?php include hji_theme_template_path(); ?>
                        
                        <?php do_action( 'hji_theme_after_content_col' ); ?>
                        
                        <div class="row">
                        
                        <div class="col-md-3 home-value">
                            
                            <?php dynamic_sidebar( 'slider-widget'); ?>
                        </div>

                        <div class="col-md-9 map"> <?php get_template_part( 'templates/map' ); ?> </div>

                    </div>

                </div>
            
            </section>
            
            <div class="search-widget row">
                 
                 <div class="col-sm-12 col-md-10 col-lg-9 center">
                 
                    <?php dynamic_sidebar( 'home-search-widget'); ?>
                        
                 </div>
                 
            </div>
            
            <?php do_action( 'hji_theme_after_primary' ); ?>

            
            <section class="container">

                <div class="row">

                   <?php get_template_part( 'templates/cta-boxes' ); ?>
                
                </div>
            
            </section>

            
            <?php do_action( 'hji_theme_after_primary' ); ?>

            
          <?php if ( is_active_sidebar( 'blvd-homepageparallax' ) ) : ?>

                    <div class="homepage-parallax">

                        <div class="container">

                            <div class="blvd-homepage-parallax row">

                                <?php dynamic_sidebar( 'blvd-homepageparallax'); ?>

                            </div>

                        </div>

                    </div>

            <?php endif; ?>            

            <section class="container">
            
                <div class="col-md-8 news">
                    
                    <h2 class="news-cat">* Latest News</h2>                    
                         <?php query_posts('posts_per_page=3'); if (have_posts()) : while (have_posts()) : the_post(); ?>     
                         <?php get_template_part( 'templates/entry-home' ); ?>
               
                         <?php endwhile; ?> <?php endif; ?>
                         
                          <?php wp_reset_query(); ?> 
                          
                        <a href ="/latest-market-news/">View Entire Blog <i class="fa fa-angle-double-right"></i>
 </a>                
                </div>
                            
                <div class="col-md-4">
              
                    <?php if ( is_active_sidebar( 'blvd-homewidgets' ) ) : ?>

                    <div class="blvd-home-widgets row">
                        <?php dynamic_sidebar( 'blvd-homewidgets'); ?>
                    </div>

                <?php endif; ?>        
                   
                </div>            

            </section>

            <?php get_footer( hji_theme_template_base() ); ?>

        </div>

    </body>

</html>