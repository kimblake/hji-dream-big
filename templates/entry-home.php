<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php do_action( 'hji_theme_before_entry' ); ?>

    <?php if ( has_action( 'hji_themework_featured_media' /* deprecated */ ) || has_action( 'hji_theme_featured_media' ) ) : 

        do_action( 'hji_themework_featured_media' ); // deprecated
        do_action( 'hji_theme_featured_media' );

    elseif ( has_post_thumbnail() ) : ?>

        <div class="entry-thumb">

            <?php echo hji_theme_featured_image(); ?>

        </div>

    <?php endif; ?>

    <header class="entry-header">

        <?php get_template_part( 'templates/entry-title' ); ?>
        <?php get_template_part( 'templates/entry-meta' ); ?>

    </header>

    <div class="entry-content">

        <?php echo apply_filters( 'hji_theme_do_the_excerpt', get_the_excerpt() ); ?>

    </div>
    


    <?php if ( !is_search() ) get_template_part( 'templates/entry-footer' ); ?>

    <?php do_action( 'hji_theme_after_entry' ); ?>

</article>