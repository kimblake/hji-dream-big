<header class="entry-title">

    <?php get_template_part( 'templates/entry-title' ); ?>
    
    <?php get_template_part( 'templates/entry-meta' ); ?>

</header>

<section class="entry-content">

    <?php the_content(); ?>

    <div class="entry-links">

        <?php wp_link_pages(); ?>

    </div>
    
</section>

<footer>

    <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>

    <?php do_action( 'hji_theme_before_comments' ); ?>

    <?php comments_template('/templates/comments.php'); ?>

    <?php do_action( 'hji_theme_after_comments' ); ?>

</footer>